Welcome to the Block Party, a Vim plugin that helps you select Python code blocks.

The goal of this plugin is to make is easy and simple to manipulate self-contained
pieces of Python code.

For example, how would you go about deleting each of these blocks of code in Vim, normally?

```python
while True:
	print('doing stuff')

	time.sleep(1)
```

```python
if foo():
	print('Got foo')
elif bar():
	print('Got bar')
else:
	print('Game over man! Game over!')
```

```python
for index in range(10):
	if index in self.thing:
		print('breaking')

		run_something()

		break
else:
	print('Did not break')

```

```python
try:
	some_function()

	self.foo = 8
except ValueError:
	# Don't worry about it
	pass
except TypeError:
	# You ain't my type!
else:
	print('you get the point')

	print('the block is complicated')
finally:
	pass
```

With Block Party, to delete any of the blocks listed above, the answer
is always this: `dab`.

TODO: Insert dab image

Block Party gives you a "one-size-fits-all" text object that works correctly,
no matter what block of Python code you're editting.


## Requirements
The Python code parser, [parso](https://pypi.org/project/parso/)
The plugin which creates the text object, [vim-textobj-user](https://github.com/kana/vim-textobj-user)


## Installation
1. Install every requirement in the "Requirements" section.
2. Add vim-block-party using your favorite Vim package manager.

Example: [vim-plug](https://github.com/junegunn/vim-plug)

```
Plug 'ColinKennedy/vim-block-party'
```

That's all you need to get started. If you'd like to customize your experience
beyond the basic default settings, check out "Opt-in Features" and "Configuration"
for more details.


## Features
### Mappings
ib - Selects around block (no whitespace collapse)
ab - Selects around block (will whitespace collapse)

iB - Selects around block (no whitespace collapse) (don't take into account config)
aB - Selects around block (will whitespace collapse) (don't take into account config)


- TODO : Make sure that searching doesn't grab anything below the block


### Opt-in Features

TODO :
	Consider making block-specific mappings
	like "iwb - Selects the nearest outer while block"


Features
- Select the nearest block type (like the nearest while loop)
- Or the nearest whatever type (automatic)

- Have option to include same-indent neighboring code, if the names match


## Configuration
Block Party is set to sensible defaults but, if you work a different way or
have different coding habits, Block Party comes with many settings to help you
fine-tune the tool to the way you like to work.


## Similar Plugins
TODO : Put links to the other plugins that create similar text objects.


## TODO
- Write tests for whitespace
- Implement the `Have option to include same-indent neighboring code, if the names match` feature
	- Write tests for this feature
